//
//  TrimVideoViewController.swift
//  VideoEditor
//
//  Created by Bá Anh Nguyễn on 11/6/17.
//  Copyright © 2017 Bá Anh Nguyễn. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import MobileCoreServices
import CoreMedia
import AssetsLibrary
import Photos
import SVProgressHUD

protocol TrimVideoViewControllerDelegate {
    func didFinishTrimming(_ viewController: TrimVideoViewController, sourceURL: URL, outputURL: URL)
}

class TrimVideoViewController: BaseViewController {
    var btnDone: UIBarButtonItem!
    var btnDiscard: UIBarButtonItem!
    
    @IBOutlet weak var vPlayer: UIView!
    @IBOutlet weak var videoLayer: UIView!
    
    @IBOutlet weak var frameContainerView: UIView!
    @IBOutlet weak var imageFrameView: UIView!
    
    @IBOutlet weak var startTimeText: UITextField!
    @IBOutlet weak var endTimeText: UITextField!
    
    var delegate: TrimVideoViewControllerDelegate?
    
    var isPlaying = true
    var isSliderEnd = true
    var playbackTimeCheckerTimer: Timer! = nil
    let playerObserver: Any? = nil
    
    let exportSession: AVAssetExportSession! = nil
    var player: AVPlayer!
    var playerItem: AVPlayerItem!
    var playerLayer: AVPlayerLayer!
    var asset: AVURLAsset!
    var sourceURL: URL!
    
    var url:NSURL! = nil
    var startTime: CGFloat = 0.0
    var stopTime: CGFloat  = 0.0
    var thumbTime: CMTime!
    var thumbtimeSeconds: Double!
    
    var videoPlaybackPosition: CGFloat = 0.0
    var cache:NSCache<AnyObject, AnyObject>!
    var rangeSlider: RangeSlider! = nil
    
    var playerVC: AVPlayerViewController!
    
    var isTrimmed = false
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Trim/Crop"
        
        // Config actions for buttons\
        btnDone = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(btnDoneHandler))
        btnDiscard = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(btnDiscardHandler))
        navigationItem.rightBarButtonItem = btnDone
        navigationItem.leftBarButtonItem = btnDiscard
        
        player = AVPlayer()
        //Allocating NsCahe for temp storage
        self.cache = NSCache()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Prepare player VC
        playerVC = AVPlayerViewController.init()
        playerVC.player = player
        
        // prepare views
        loadViews()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Remove Observers
    }
    
    func loadViews() {
        thumbTime = asset.duration
        thumbtimeSeconds      = CMTimeGetSeconds(thumbTime)
        
        self.viewAfterVideoIsPicked()
        
        let item:AVPlayerItem = AVPlayerItem(asset: asset)
        player                = AVPlayer(playerItem: item)
        playerLayer           = AVPlayerLayer(player: player)
        playerLayer.frame     = videoLayer.bounds
        
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspect //AVLayerVideoGravityResizeAspectFill
        player.actionAtItemEnd   = AVPlayerActionAtItemEnd.none
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapOnVideoLayer))
        self.videoLayer.addGestureRecognizer(tap)
        self.tapOnVideoLayer(tap: tap)
        
        videoLayer.layer.addSublayer(playerLayer)
        player.play()
    }
    
    /// Dismiss a vc or a vc emmbed in a navigation controller
    func dismissVC() {
        if let nav = navigationController {
            nav.dismiss(animated: true, completion: nil)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func btnDiscardHandler() {
//        let alert = UIAlertController(title: "Confirmation", message: "Finish and go back?", preferredStyle: .alert)
//        alert.addAction(UIAlertAction.init(title: "Yes", style: .destructive, handler: { (_) in
//            if self.isTrimmed {
//                self.delegate?.didFinishTrimming(self, sourceURL: self.sourceURL, outputURL: self.asset.url)
//            }
//            self.dismiss(animated: true, completion: nil)
//        }))
//        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
//        self.present(alert, animated: true, completion: nil)
        
        dismissVC()
    }
    
    @objc func btnDoneHandler() {
        
        let alert = UIAlertController(title: "Confirmation", message: "Apply trimming with the range you selected, your video will be processed and it cannot be undo.", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Yes", style: .default, handler: { (action) in
            if let startText = self.startTimeText.text,
                let endText = self.endTimeText.text,
                let startSecond = Util.computeTimeString(with: startText),
                let endSecond = Util.computeTimeString(with: endText)
            {
                self.trimVideo(startTime: startSecond, endTime: endSecond)
            }
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func config(_ asset: AVURLAsset) {
        sourceURL = asset.url
        self.asset = asset
        player = AVPlayer.init(playerItem: AVPlayerItem.init(asset: self.asset))
    }
    
    func config(withURL url: URL) {
        sourceURL = url
        self.asset = AVURLAsset.init(url: url)
        player = AVPlayer.init(playerItem: AVPlayerItem.init(asset: self.asset))
    }
    
    
}

extension TrimVideoViewController: UINavigationControllerDelegate,UITextFieldDelegate {
    func createRangeSlider() {
        //Remove slider if already present
        let subViews = self.frameContainerView.subviews
        for subview in subViews {
            if subview.tag == 1000 {
                subview.removeFromSuperview()
            }
        }
        
        rangeSlider = RangeSlider(frame: frameContainerView.bounds)
        frameContainerView.addSubview(rangeSlider)
        rangeSlider.tag = 1000
        //Range slider action
        rangeSlider.addTarget(self, action: #selector(rangeSliderValueChanged(_:)), for: .valueChanged)
        
        let time = DispatchTime.now() + Double(Int64(NSEC_PER_SEC)) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: time) {
            self.rangeSlider.trackHighlightTintColor = UIColor.clear
            self.rangeSlider.curvaceousness = 1.0
        }
        
    }
    
    func rangeSliderValueChanged(_ rangeSlider: RangeSlider) {
        self.player.pause()
        
        if isSliderEnd {
            rangeSlider.minimumValue = 0.0
            rangeSlider.maximumValue = Double(thumbtimeSeconds)
            rangeSlider.upperValue = Double(thumbtimeSeconds)
            isSliderEnd = !isSliderEnd
        }
        
        if rangeSlider.upperValue - rangeSlider.lowerValue < 3.0 { // 3 seconds length
            return
        }
        
        startTimeText.text = Util.populateTime(with: rangeSlider.lowerValue, decimalSecond: true)
        endTimeText.text   = Util.populateTime(with: rangeSlider.upperValue, decimalSecond: true)
        
        if rangeSlider.lowerLayerSelected {
            self.seekVideo(toPos: CGFloat(rangeSlider.lowerValue))
        } else {
            self.seekVideo(toPos: CGFloat(rangeSlider.upperValue))
        }
    }
    
    
    //MARK: TextField Delegates
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        let maxLength     = 3
        let currentString = startTimeText.text! as NSString
        let newString     = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    //Seek video when slide
    func seekVideo(toPos pos: CGFloat) {
        self.videoPlaybackPosition = pos
        let time: CMTime = CMTimeMakeWithSeconds(Float64(self.videoPlaybackPosition), self.player.currentTime().timescale)
        self.player.seek(to: time, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
        
        if (pos == CGFloat(thumbtimeSeconds)) {
            self.player.pause()
        }
    }
    
    //Trim Video Function, time: seconds
    func trimVideo(startTime: Double, endTime: Double) {
        let length = Float(asset.duration.value) / Float(asset.duration.timescale)
        print("video length: \(length) seconds")
        
        let currentAssetURL = asset.url
        let outputExtension = currentAssetURL.pathExtension
        let outputFileType = Util.fileType(from: outputExtension)
        let outputURL = currentAssetURL.deletingLastPathComponent().appendingPathComponent(UUID().uuidString + "." + outputExtension)
        
        
        guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) else { return }
        let timeRange = CMTimeRange(start: CMTime(seconds: startTime, preferredTimescale: 1000), end: CMTime(seconds: endTime, preferredTimescale: 1000))
        exportSession.outputURL = outputURL
        exportSession.outputFileType = outputFileType
        exportSession.timeRange = timeRange
        
        exportSession.exportAsynchronously {
            switch exportSession.status {
            case .waiting:
                SVProgressHUD.showInfo(withStatus: "Waiting...")
                break
                
            case .exporting:
                SVProgressHUD.showProgress(exportSession.progress, status: "Trimming Video...")
                break
                
            case .completed:
                print("exported at \(outputURL)")
                // Replace new one with the current url
                do {
                    try FileManager.default.removeItem(at: currentAssetURL)
                    try FileManager.default.moveItem(at: outputURL, to: currentAssetURL)
                    let asset = AVURLAsset.init(url: currentAssetURL)
                    self.config(asset)
                    DispatchQueue.main.async {
                        self.loadViews()
                    }
                    SVProgressHUD.showSuccess(withStatus: "Trimming Done")
                    self.isTrimmed = true
                    
                    self.dismiss(animated: true, completion: {
                        self.delegate?.didFinishTrimming(self, sourceURL: self.sourceURL, outputURL: self.asset.url)
                    })
                } catch {
                    SVProgressHUD.showError(withStatus: "File handler error")
                }
                break
                
            case .failed:
                SVProgressHUD.showError(withStatus: "Failed")
                print("failed \(exportSession.error)")
                break
                
            case .cancelled:
                SVProgressHUD.showError(withStatus: "Cancelled")
                print("cancelled \(exportSession.error)")
                break
                
            default:
                SVProgressHUD.dismiss()
                break
            }
        }
    }
    
    func viewAfterVideoIsPicked() {
        //Rmoving player if alredy exists
        if(playerLayer != nil) {
            playerLayer.removeFromSuperlayer()
        }
        
        self.createImageFrames()
        
        //unhide buttons and view after video selection
//        btnDone.isEnabled         = false
        frameContainerView.isHidden = false
        
        
        isSliderEnd = true
//        startTimeText.text! = "\(0.0)"
//        endTimeText.text   = "\(thumbtimeSeconds!)"
        startTimeText.text = Util.populateTime(with: 0.0)
        endTimeText.text = Util.populateTime(with: Double(thumbtimeSeconds), decimalSecond: true)
        self.createRangeSlider()
    }
    
    //Tap action on video player
    func tapOnVideoLayer(tap: UITapGestureRecognizer) {
        if isPlaying {
            self.player.play()
        } else {
            self.player.pause()
        }
        isPlaying = !isPlaying
    }
    
    
    
    //MARK: CreatingFrameImages
    func createImageFrames() {
        //creating assets
        let assetImgGenerate : AVAssetImageGenerator    = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        assetImgGenerate.requestedTimeToleranceAfter    = kCMTimeZero;
        assetImgGenerate.requestedTimeToleranceBefore   = kCMTimeZero;
        
        assetImgGenerate.appliesPreferredTrackTransform = true
        let thumbTime: CMTime = asset.duration
        let thumbtimeSeconds  = Int(CMTimeGetSeconds(thumbTime))
        let maxLength         = "\(thumbtimeSeconds)" as NSString
        
        let thumbAvg  = Double(thumbtimeSeconds) / 6.0
        var startTime = 1.0
        var startXPosition:CGFloat = 0.0
        
        //loop for 6 number of frames
        for _ in 0...5 {
            
            let imageButton = UIButton()
            let xPositionForEach = CGFloat(self.imageFrameView.frame.width)/6
            imageButton.frame = CGRect(x: CGFloat(startXPosition), y: CGFloat(0), width: xPositionForEach, height: CGFloat(self.imageFrameView.frame.height))
            do {
                let time:CMTime = CMTimeMakeWithSeconds(Float64(startTime),Int32(maxLength.length))
                let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
                let image = UIImage(cgImage: img)
                imageButton.setImage(image, for: .normal)
            }
            catch
                _ as NSError
            {
                print("Image generation failed with error (error)")
            }
            
            startXPosition = startXPosition + xPositionForEach
            startTime = startTime + thumbAvg
            imageButton.isUserInteractionEnabled = false
            imageFrameView.addSubview(imageButton)
        }
        
    }
}
