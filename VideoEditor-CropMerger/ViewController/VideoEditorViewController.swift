//
//  VideoEditorViewController.swift
//  VideoEditor-CropMerger
//
//  Created by Bá Anh Nguyễn on 12/13/17.
//  Copyright © 2017 Bá Anh Nguyễn. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import MobileCoreServices
import TZImagePickerController
import SVProgressHUD

class VideoEditorViewController: BaseViewController {
    
    @IBOutlet weak var viewClipBar: UIView!
    @IBOutlet weak var cvClip: UICollectionView!
    
    @IBOutlet weak var viewPlayerContainer: UIView!
    @IBOutlet weak var viewVideoItemContainer: UIView!
    @IBOutlet weak var viewToolItemContainer: UIView!
    @IBOutlet weak var lbTimer: UILabel!
    @IBOutlet weak var btnPlayPause: UIButton!
    @IBOutlet weak var btnAddVideo: UIButton!
    
    // -- view
    var viewImportMenu: ImportMenuView!
    var viewVideoToolMenu: VideoToolMenuView!
    
    fileprivate var videoUrlArray = [URL]()
    fileprivate var player: AVPlayer!
    fileprivate var playerVC: AVPlayerViewController!
    fileprivate var compositionAsset: AVAsset! {
        didSet {
            if nil != cvClip {
                self.refreshClips()
            }
        }
    }
    
    fileprivate var clips = [UIImage]()
    fileprivate var cellReuseId = ""
    fileprivate let cellSize = CGSize.init(width: 60, height: 60)
    fileprivate let halfScreenWidth = UIScreen.main.bounds.width / 2.0
    fileprivate var clipInterval = 3000 // ms
    fileprivate var previousAssetOffsetX:CGFloat = 0.0
    
    fileprivate var isManuallyScrollingClips = false
    
    
    // ----
    @IBOutlet weak var stackVideosView: UIStackView!
    fileprivate var willBeSelectedItemIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Video Editor"
        
        btnPlayPause.addTarget(self, action: #selector(btnPlayPauseHandler), for: .touchUpInside)
        btnAddVideo.addTarget(self, action: #selector(btnAddVideoHandler), for: .touchUpInside)
        
        let btnExport = UIBarButtonItem.init(title: "Export", style: .done, target: self, action: #selector(btnExportHandler))
        navigationItem.rightBarButtonItem = btnExport
        
        preparePlayer()
        refreshClips()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: nil) { [weak self] (note) in
            guard let this = self, let currentAsset = this.player.currentItem?.asset as? AVURLAsset else { return }
        }
        
        btnAddVideoHandler()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let player = player, player.isPlaying {
            player.pause()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
    }
    
    override func prepareUI() {
        super.prepareUI()
        
        viewClipBar.backgroundColor = .clear
        
        lbTimer.text = Util.populateTime(with: 0)
        lbTimer.font = UIFont.myFontLight(ofSize: 12)
        lbTimer.textColor = UIColor.Palette.detail
        
        viewPlayerContainer.backgroundColor = .clear
        viewToolItemContainer.backgroundColor = .clear
        viewVideoItemContainer.backgroundColor = .clear
        
        btnPlayPause.layer.masksToBounds = true
        btnPlayPause.heightAnchor.constraint(equalToConstant: 80).isActive = true
        btnPlayPause.widthAnchor.constraint(equalToConstant: 80).isActive = true
        btnPlayPause.layer.cornerRadius = 40.0
        btnPlayPause.backgroundColor = UIColor.Palette.primary.withAlphaComponent(0.5)
        btnPlayPause.tintColor = UIColor.clear
        
        
        viewImportMenu = ImportMenuView.init(frame: viewToolItemContainer.bounds)
        viewImportMenu.delegate = self
        
        viewVideoToolMenu = VideoToolMenuView.init(frame: viewToolItemContainer.bounds)
        viewVideoToolMenu.delegate = self
    }
    
    public func configAddVideo(_ url: URL) {
        if !videoUrlArray.contains(url) {
            videoUrlArray.append(url)
            compositionAsset = makeComposition(from: videoUrlArray)
        }
        // Update UI if needed
    }
    
    public func configVideoURLs(_ urls: [URL]) {
        videoUrlArray.removeAll()
        urls.forEach { (url) in
            if !videoUrlArray.contains(url) {
                videoUrlArray.append(url)
            }
        }
        compositionAsset = makeComposition(from: videoUrlArray)
    }
    
    
    func btnPlayPauseHandler() {
        guard let player = player else { return }
        if player.isPlaying {
            player.pause()
        } else {
            player.play()
        }
        btnPlayPauseUpdate()
    }
    
    func btnAddVideoHandler() {
//        viewToolItemContainer.subviews.forEach { (v) in
//            v.removeFromSuperview()
//        }
//
//        viewToolItemContainer.layout(viewImportMenu).edges()
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseInOut, animations: {
            self.viewToolItemContainer.subviews.forEach { (v) in
                v.alpha = 0
            }
        }) { (done) in
            if done {
                self.viewToolItemContainer.subviews.forEach { (v) in
                    v.removeFromSuperview()
                }
                self.viewToolItemContainer.layout(self.viewImportMenu).edges()
                self.viewToolItemContainer.subviews.forEach { (v) in
                    v.alpha = 1
                }
            }
        }
    }
    
    func btnPlayPauseUpdate() {
        if player == nil {
            btnPlayPause.setImage(#imageLiteral(resourceName: "play_filled"), for: .normal)
        } else {
            if player.isPlaying {
                btnPlayPause.setImage(#imageLiteral(resourceName: "pause_filled"), for: .normal)
            } else {
                btnPlayPause.setImage(#imageLiteral(resourceName: "play_filled"), for: .normal)
            }
        }
    }
    
    func btnExportHandler() {
        let vc = ExportViewController()
        vc.config(data: self.compositionAsset)
        self.navigationController?.pushViewController(vc, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: - Private methods
    fileprivate func makeComposition(from urls: [URL]) -> AVMutableComposition {
        
        let assetArray = urls.map { AVAsset.init(url: $0 as! URL) }
        var lastDuration = kCMTimeZero
        
        let mixComposition = AVMutableComposition()
        let compositionVideoTrack = mixComposition.addMutableTrack(withMediaType: AVMediaTypeVideo,
                                                                   preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        let compositionAudioTrack = mixComposition.addMutableTrack(withMediaType: AVMediaTypeAudio,
                                                                   preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        
        for i in 0..<assetArray.count {
            let asset = assetArray[i]
            NSLog("Asset %d Video Tracks: %@", i, asset.tracks(withMediaType: AVMediaTypeVideo).debugDescription)
            NSLog("Asset %d Audio Tracks: %@", i, asset.tracks(withMediaType: AVMediaTypeAudio).debugDescription)
            do {
                let videoTrack = asset.tracks(withMediaType: AVMediaTypeVideo)[0]
                try compositionVideoTrack.insertTimeRange(CMTimeRangeMake(kCMTimeZero, asset.duration),
                                                          of: videoTrack, at: lastDuration)
                //                compositionVideoTrack.preferredTransform = videoTrack.preferredTransform
                
                let audioTrack = asset.tracks(withMediaType: AVMediaTypeAudio)[0]
                try compositionAudioTrack.insertTimeRange(CMTimeRangeMake(kCMTimeZero, asset.duration),
                                                          of: audioTrack, at: lastDuration)
            } catch let error as NSError {
                print("error: \(error)")
            }
            lastDuration = CMTimeAdd(lastDuration, asset.duration)
        }
        
        return mixComposition
    }
    
    
    fileprivate func prepapreClips(from asset: AVAsset, clipInterval milisecond: Int = 1000) {
        clips.removeAll()
        let clipImages = AssetHelper.shared.exportClips(fromAsset: asset, withSize: cellSize, miliSecondsInterval: milisecond, startFirstClipAt: 0)
        self.clips.append(contentsOf: clipImages)
    }
    
    fileprivate func prepareCollectionView() {
        cellReuseId = "\(ClipCollectionViewCell.self)"
        cvClip.register(UINib.init(nibName: cellReuseId, bundle: nil), forCellWithReuseIdentifier: cellReuseId)
        let halfWidth = UIScreen.main.bounds.width / 2.0
        cvClip.contentInset = UIEdgeInsets.init(top: 0, left: halfWidth, bottom: 0, right: halfWidth)
        cvClip.decelerationRate = 0.0
        cvClip.bounces = false
        cvClip.delegate = self
        cvClip.dataSource = self
        
        let panGesture = UIPanGestureRecognizer.init(target: self, action: #selector(handlePanGestureForCollectionView(gesture:)))
        panGesture.delegate = self
        cvClip.addGestureRecognizer(panGesture)
    }
    
    @objc fileprivate func handlePanGestureForCollectionView(gesture: UIGestureRecognizer) {
        if gesture.state == .began {
            isManuallyScrollingClips = true
        }
        if gesture.state == .ended {
            isManuallyScrollingClips = false
        }
    }
    
    fileprivate func refreshClips() {
        self.prepapreClips(from: compositionAsset, clipInterval: self.clipInterval)
        self.prepareCollectionView()
        self.cvClip.reloadData()
        
        unloadPlayer()
        preparePlayer()
    }
    
    fileprivate func prepareVideoStackView(with assetUrlArray: [URL]) {
        NSLog("prepare stackView with assetArray")
        guard let stackView = self.stackVideosView else { return }
        for v in stackView.arrangedSubviews { // Clear stack
            v.removeFromSuperview()
        }
        for index in 0..<assetUrlArray.count { // Add new subviews
            let v = VideoAssetView()
            v.config(assetURL: assetUrlArray[index], order: index + 1, shouldShowDeleteButton: false)
            v.tag = index // IMPORTANT, use to find new index after reordering in `stackViewDidEndReordering`.
            v.widthAnchor.constraint(equalToConstant: 64).isActive = true
            v.delegate = self
            v.setSelected(willBeSelectedItemIndex == index)
            stackView.addArrangedSubview(v)
        }
    }
    
    fileprivate func refreshVideoStackView() {
        guard !self.videoUrlArray.isEmpty else { return }
        prepareVideoStackView(with: self.videoUrlArray)
    }
    
    fileprivate func preparePlayer() {
        if player == nil, compositionAsset != nil, !videoUrlArray.isEmpty {
            player = AVPlayer.init(playerItem: AVPlayerItem.init(asset: compositionAsset))
            
            let durationString = Util.populateTime(with: CMTimeGetSeconds(player.currentItem?.duration ?? kCMTimeZero))
            player.addPeriodicTimeObserver(forInterval: CMTime.init(value: 1, timescale: 1), queue: .main) { [weak self] time in
                guard let this = self else { return }
                let timeString = Util.populateTime(with: time.seconds) + " / " + durationString
                this.lbTimer.text = timeString
                
            } // .addPeriodicTimeObserver
            
            player.addPeriodicTimeObserver(forInterval: CMTime.init(value: 1, timescale: 24), queue: .main, using: { [weak self] (time) in
                guard let this = self else { return }
                if !this.isManuallyScrollingClips {
                    let xOffsetScrollView = this.previousAssetOffsetX + CGFloat(time.seconds) * this.cellSize.width * 1000.0 / CGFloat(this.clipInterval) - this.halfScreenWidth
                    this.cvClip.setContentOffset(CGPoint.init(x: xOffsetScrollView, y: 0), animated: false)
                }
            })
            
            playerVC = AVPlayerViewController()
            playerVC.player = player
            playerVC.updatesNowPlayingInfoCenter = false
            playerVC.allowsPictureInPicturePlayback = false
            playerVC.view.isUserInteractionEnabled = false
            playerVC.showsPlaybackControls = false
            playerVC.view.frame = viewPlayerContainer.bounds
            addChildViewController(playerVC)
            viewPlayerContainer.addSubview(playerVC.view)
        }
        btnPlayPauseUpdate()
        
        prepareVideoStackView(with: self.videoUrlArray)
        
    }
    
    fileprivate func unloadPlayer() {
        if nil != player {
            if player.isPlaying {
                player.pause()
            }
            viewPlayerContainer.subviews.forEach { $0.removeFromSuperview() }
            player = nil
            playerVC = nil
        }
        btnPlayPauseUpdate()
    }
}

extension VideoEditorViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let videoUrl = info[UIImagePickerControllerMediaURL] as? URL {
            self.dismiss(animated: true, completion: {
                //self.performSegue(withIdentifier: self.segueId, sender: videoUrl)
                
                // Copy to Temporary folder
                let destinationVideoURL = URL.init(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true).appendingPathComponent(UUID.init().uuidString + "." + videoUrl.pathExtension)
                DispatchQueue.global(qos: .background).async {
                    do {
                        try FileManager.default.copyItem(at: videoUrl, to: destinationVideoURL)
                        NSLog("Copied video to %@", destinationVideoURL.path)
                        DispatchQueue.main.async {
                            self.configAddVideo(destinationVideoURL)
                        }
                    } catch let error as NSError {
                        NSLog("Error on copying video asset: %@", error.debugDescription)
                    }
                }
                
            })
        }
    }
}

extension VideoEditorViewController: ImportMenuViewDelegate {
    func menuViewDidPressCameraButton(_ menu: ImportMenuView) {
        NSLog("menuViewDidPressCameraButton")
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let videoPicker = UIImagePickerController()
            videoPicker.delegate = self
            videoPicker.sourceType = .camera
            videoPicker.mediaTypes = [kUTTypeMovie as String]
            videoPicker.allowsEditing = true
            videoPicker.showsCameraControls = true
            self.present(videoPicker, animated: true, completion: nil)
        } else {
            print("camera not available.")
        }
    }
    
    func menuViewDidPressLibraryButton(_ menu: ImportMenuView) {
        if let imagePickerVC = TZImagePickerController.init(maxImagesCount: 9, columnNumber: 4, delegate: self) {
            imagePickerVC.allowPickingMultipleVideo = true
            imagePickerVC.allowPickingVideo     = true
            imagePickerVC.allowPickingImage     = false
            imagePickerVC.allowPickingGif       = false
            imagePickerVC.cancelBtnTitleStr     = "Cancel"
            imagePickerVC.doneBtnTitleStr       = "Done"
            imagePickerVC.previewBtnTitleStr    = "Preview"
            imagePickerVC.fullImageBtnTitleStr  = "Full Size"
            imagePickerVC.processHintStr        = "Loading"
            imagePickerVC.settingBtnTitleStr    = "Settings"
            self.present(imagePickerVC, animated: true, completion: nil)
        }
    }
    
    func menuViewDidPressArrangeButton(_ menu: ImportMenuView) {
        let vc = ReorderVideoViewController()
        vc.config(withUrlArray: self.videoUrlArray)
        vc.delegate = self
        let nav = BaseNavigationController.init(rootViewController: vc)
        self.present(nav, animated: true, completion: nil)
    }
}

extension VideoEditorViewController: VideoToolMenuViewDelegate, TrimVideoViewControllerDelegate, ReorderVideoViewControllerDelegate, RotateVideoViewControllerDelegate {
    
    // MARK: - VideoToolMenuViewDelegate
    func menuViewDidPressRotateButton(_ menu: VideoToolMenuView, for url: URL?) {
        let alert = UIAlertController.init(title: "How do you want to rotate?", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction.init(title: "Selected Video", style: .default, handler: { (_) in
            let vc = RotateVideoViewController()
            let selectedURL = self.videoUrlArray[self.willBeSelectedItemIndex]
            vc.config(withUrlArray: [selectedURL])
            vc.delegate = self
            let nav = BaseNavigationController.init(rootViewController: vc)
            self.present(nav, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction.init(title: "All Videos", style: .default, handler: { (_) in
            let vc = RotateVideoViewController()
            vc.config(withUrlArray: self.videoUrlArray) // `url` arg is not neccessary here, we load all videos
            vc.delegate = self
            let nav = BaseNavigationController.init(rootViewController: vc)
            self.present(nav, animated: true, completion: nil)
        }))
        present(alert, animated: true, completion: nil)
        
        
        
    }
    
    func menuViewDidPressClipButton(_ menu: VideoToolMenuView, for url: URL?) {
        NSLog("Start clipping for %@", url?.path ?? "----")
        if let url = url {
            let vc = TrimVideoViewController()
            vc.config(withURL: url)
            vc.delegate = self
            let nav = BaseNavigationController.init(rootViewController: vc)
            self.present(nav, animated: true, completion: nil)
        }
    }
    
//    func menuViewDidPressSpeedButton(_ menu: VideoToolMenuView, for url: URL?) {
//        
//    }
//    
//    func menuViewDidPressVolumeButton(_ menu: VideoToolMenuView, for url: URL?) {
//        
//    }
    
    // MARK: - TrimVideoViewControllerDelegate
    func didFinishTrimming(_ viewController: TrimVideoViewController, sourceURL: URL, outputURL: URL) {
        NSLog("didFinishTrimming\n source: %@, target: %@", sourceURL.path, outputURL.path)
        // often: sourceURL == outputURL
        compositionAsset = makeComposition(from: videoUrlArray)
        unloadPlayer()
        preparePlayer()
    }
    
    // MARK: - ReorderVideoViewControllerDelegate
    func didFinishArrange(_ viewController: ReorderVideoViewController, sourceUrlArray: [URL], outputUrlArray: [URL]) {
        self.configVideoURLs(outputUrlArray)
        compositionAsset = makeComposition(from: videoUrlArray)
        unloadPlayer()
        preparePlayer()
    }
    
    // MARK: - RotateVideoViewControllerDelegate
    func didFinishRotate(_ viewController: RotateVideoViewController, sourceUrlArray: [URL], outputUrlArray: [URL]) {
        self.configVideoURLs(outputUrlArray)
        compositionAsset = makeComposition(from: videoUrlArray)
        unloadPlayer()
        preparePlayer()
    }
    
    func didFinishRotate(_ viewController: RotateVideoViewController, sourceUrl: URL, outputUrl: URL) {
        if let index = videoUrlArray.index(of: sourceUrl) {
            videoUrlArray[index] = outputUrl
            self.configVideoURLs(videoUrlArray) // replace
            compositionAsset = makeComposition(from: videoUrlArray)
            unloadPlayer()
            preparePlayer()
        }
        
    }
}

extension VideoEditorViewController: VideoAssetViewDelegate {
    func didTap(_ view: VideoAssetView, assetURL: URL) {
        let index = view.tag
        
        willBeSelectedItemIndex = index
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: .transitionCrossDissolve, animations: {
            self.viewToolItemContainer.subviews.forEach { (v) in
                v.alpha = 0
            }
        }) { (done) in
            if done {
                self.viewToolItemContainer.subviews.forEach { (v) in
                    v.removeFromSuperview()
                }
                self.viewVideoToolMenu.sendingURL = self.videoUrlArray[index]
                self.viewToolItemContainer.layout(self.viewVideoToolMenu).edges()
                self.viewToolItemContainer.subviews.forEach { (v) in
                    v.alpha = 1
                }
                self.refreshVideoStackView()
            }
        }
        
        
        
    }
    
    func didSelectDelete(_ view: VideoAssetView, assetURL: URL) {
        // NO need
    }
}


// MARK: - Gesture Delegates
extension VideoEditorViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true // prevent clip collectionview stop when manually scroll.
    }
}

// MARK: - Collection View Delegates & Datasource
extension VideoEditorViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return clips.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let vClip = ClipView.init()
        vClip.config(with: clips[indexPath.item], at: Double(indexPath.item * clipInterval) / 1000.0)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseId, for: indexPath)
        cell.layout(vClip).bottom().left().right().top()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldDeselectItemAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let player = player else { return }
        if isManuallyScrollingClips { // allowed by pan gesture handler before, at this time, player is paused for manual scrolling.
            let assetOffsetX = scrollView.contentOffset.x - self.previousAssetOffsetX + self.halfScreenWidth
            let miliseconds = Int64(assetOffsetX / cellSize.width * CGFloat(clipInterval))
            let timeInSecond = CMTime.init(value: miliseconds, timescale: 1000)
            player.seek(to: timeInSecond, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero, completionHandler: { (didSeek) in
                player.pause()
                self.btnPlayPauseUpdate()
            })
        } else {
            //NSLog("Scroll Automatically")
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        guard player != nil else { return }
        self.player.pause()
    }
}


extension VideoEditorViewController: TZImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: TZImagePickerController!, didFinishPickingPhotos photos: [UIImage]!, sourceAssets assets: [Any]!, isSelectOriginalPhoto: Bool) {
        if let assetArr = assets as? [PHAsset], AssetHelper.shared.lengthValid(assets: assetArr) {
            let dispatchGroup = DispatchGroup()
            dispatchGroup.enter()
            var completedAssetCount = 0
            var maxAssetCount = assetArr.count
            var completedURLs = [URL]()
            assetArr.forEach({ (asset) in
                PHImageManager.init().requestAVAsset(forVideo: asset, options: nil, resultHandler: { (avAsset, audioMix, info) in
                    if let urlAsset = avAsset as? AVURLAsset {
                        do {
                            let sourceVideoUrl = urlAsset.url
                            NSLog("sourceVideoUrl = %@", sourceVideoUrl.path)
                            let statusString = String.init(format: "Copying %d/%d file%@", completedAssetCount + 1, maxAssetCount, maxAssetCount > 1 ? "s" : "")
                            SVProgressHUD.showInfo(withStatus: statusString)
                            let destinationVideoURL = URL.init(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true).appendingPathComponent(UUID.init().uuidString + "." + sourceVideoUrl.pathExtension)
                            try FileManager.default.copyItem(at: sourceVideoUrl, to: destinationVideoURL)
                            NSLog("Copied video to %@", destinationVideoURL.path)
                            completedURLs.append(destinationVideoURL)
                            completedAssetCount += 1
                            if completedAssetCount >= maxAssetCount {
                                dispatchGroup.leave()
                            }
                        } catch let error as NSError {
                            NSLog("Error on copying video asset: %@", error.debugDescription)
                            completedAssetCount += 1
                            
                        } // .do-catch
                    } else {
                        NSLog("Not an AVAsset")
                        SVProgressHUD.showError(withStatus: "Skipped an asset!")
                        completedAssetCount += 1
                    } // .if
                }) // .request
            }) // .for
            
            dispatchGroup.notify(queue: .main, execute: {
                SVProgressHUD.dismiss()
                var newArray = [URL]()
                newArray.append(contentsOf: self.videoUrlArray) // old urls
                newArray.append(contentsOf: completedURLs) // new copired urls
                self.configVideoURLs(newArray)
            })
        } else {
            NSLog("Asset not validated")
        }
        
    }
}


