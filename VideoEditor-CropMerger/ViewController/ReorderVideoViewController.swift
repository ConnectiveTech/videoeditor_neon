//
//  ReorderVideoViewController.swift
//  VideoEditor
//
//  Created by Bá Anh Nguyễn on 10/23/17.
//  Copyright © 2017 Bá Anh Nguyễn. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import SVProgressHUD
import SQReorderableStackView
import TZImagePickerController

enum ReorderVideoViewControllerMode {
    case newComposition
    case editComposition
}

protocol ReorderVideoViewControllerDelegate {
    func didFinishArrange(_ viewController: ReorderVideoViewController, sourceUrlArray: [URL], outputUrlArray: [URL])
}

class ReorderVideoViewController: BaseViewController {
    
    var btnAdd: UIBarButtonItem!
    var btnDone: UIBarButtonItem!
    var btnDiscard: UIBarButtonItem!
    
    @IBOutlet weak var stackVideosView: SQReorderableStackView!
    @IBOutlet weak var vPlayer: UIView!
    
    var sourceURLs = [URL]()
    var assets = [AVURLAsset]()
    var player: AVPlayer!
    var playerVC: AVPlayerViewController!
    
    var mode: ReorderVideoViewControllerMode!
    
    var delegate: ReorderVideoViewControllerDelegate?
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Arrange"
        
        btnDone = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(btnDoneHandler))
        btnAdd = UIBarButtonItem.init(title: "Import", style: .plain, target: self, action: #selector(btnAddHandler))
        btnDiscard = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(btnDiscardHandler))
        
        navigationItem.rightBarButtonItem = btnDone
        navigationItem.leftBarButtonItems = [btnDiscard, btnAdd]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        stackVideosView.reorderDelegate = self
        
        prepare(stackVideosView, with: assets)
        preparePlayer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Stop playing
        player.pause()
    }
    
    
    /// Dismiss a vc or a vc emmbed in a navigation controller
    func dismissVC() {
        if let nav = navigationController {
            nav.dismiss(animated: true, completion: nil)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func btnDiscardHandler() {
        // Ask user to add more video
        let alert = UIAlertController(title: "Confirmation", message: "Discard changes and go back?", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Yes", style: .destructive, handler: { (_) in
            self.dismissVC()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func btnAddHandler() {
        // Hanlde imgae/video picker
        if let imagePickerVC = TZImagePickerController.init(maxImagesCount: 9, columnNumber: 4, delegate: self) {
            imagePickerVC.allowPickingMultipleVideo = true
            imagePickerVC.allowPickingVideo     = true
            imagePickerVC.allowPickingImage     = false
            imagePickerVC.cancelBtnTitleStr     = "Cancel"
            imagePickerVC.doneBtnTitleStr       = "Done"
            imagePickerVC.previewBtnTitleStr    = "Preview"
            imagePickerVC.fullImageBtnTitleStr  = "Full Size"
            imagePickerVC.processHintStr        = "Loading"
            imagePickerVC.settingBtnTitleStr    = "Settings"
            self.present(imagePickerVC, animated: true, completion: nil)
        }
        
        // Validate video asset beforex
    }
    
    @objc func btnDoneHandler() {
        if assets.isEmpty {
            // Ask user to add more video
            let alert = UIAlertController(title: "Nothing to process", message: "Please add one or more videos to the stack.", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            self.dismissVC()
            let outputURLs = self.assets.map { $0.url }
            self.delegate?.didFinishArrange(self, sourceUrlArray: self.sourceURLs, outputUrlArray: outputURLs)
        }
    }
    
    func config(withUrlArray urls: [URL]) {
        sourceURLs = urls
        self.assets = urls.map { AVURLAsset.init(url: $0) }
    }

}

extension ReorderVideoViewController {
    
    // MARK: - Preparation
    
    func prepare(_ stackView: UIStackView, with assetArray: [AVURLAsset]) {
        NSLog("prepare stackView with assetArray")
        for v in stackView.arrangedSubviews { // Clear stack
            v.removeFromSuperview()
        }
        for index in 0..<assetArray.count { // Add new subviews
            let v = VideoAssetView()
            v.tag = index // IMPORTANT, use to find new index after reordering in `stackViewDidEndReordering`.
            v.delegate = self
            v.config(assetURL: assetArray[index].url, order: index + 1)
            v.widthAnchor.constraint(equalToConstant: stackVideosView.height).isActive = true
            stackView.addArrangedSubview(v)
        }
    }
    
    
    func preparePlayer() {
        guard let firstItem = assets.first else {
            return
        }
        let item = AVPlayerItem.init(asset: firstItem)
        player = AVPlayer.init()
        player.replaceCurrentItem(with: item)
        
        playerVC = AVPlayerViewController()
        playerVC.player = player
        playerVC.view.frame = CGRect(origin: .zero, size: vPlayer.frame.size)
        addChildViewController(playerVC)
        vPlayer.addSubview(playerVC.view)
    }
}

extension ReorderVideoViewController: VideoAssetViewDelegate {
    func didTap(_ view: VideoAssetView, assetURL: URL) {
        let item = AVPlayerItem.init(url: assetURL)
        player.replaceCurrentItem(with: item)
    }
    
    func didSelectDelete(_ view: VideoAssetView, assetURL: URL) {
        let alert = UIAlertController(title: "Confirm to delete", message: "Do you want to delete the video from the queue?", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Yes", style: .default, handler: { (_) in
            let index = view.tag //IMPORTANT
            self.assets.remove(at: index)
            self.prepare(self.stackVideosView, with: self.assets)
        }))
        alert.addAction(UIAlertAction.init(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}


// MARK: - Delegates

extension ReorderVideoViewController: SQReorderableStackViewDelegate {
    
    func stackViewDidReorderArrangedSubviews(_ stackView: SQReorderableStackView) {
        NSLog("stackViewDidReorderArrangedSubviews")
    }
    
    func stackViewDidBeginReordering(_ stackView: SQReorderableStackView) {
        NSLog("stackViewDidBeginReordering")
        if let player = self.player {
            player.pause()
        }
    }
    
    func stackViewDidEndReordering(_ stackView: SQReorderableStackView) {
        NSLog("stackViewDidEndReordering")
        
        var reorderAssetArray = [AVURLAsset]()
        for i in 0..<stackView.arrangedSubviews.count {
            let v = stackView.arrangedSubviews[i]
            reorderAssetArray.append(assets[v.tag]) // Check tag and append :))
        }
        self.assets = reorderAssetArray
        self.prepare(stackVideosView, with: assets) // Update UI (also update Ordering Label
    }
    
    func stackViewDidCancelReordering(_ stackView: SQReorderableStackView) {
        NSLog("stackViewDidCancelReordering")
    }
}

extension ReorderVideoViewController: TZImagePickerControllerDelegate {
    
    
    func imagePickerController(_ picker: TZImagePickerController!, didFinishPickingVideo coverImage: UIImage!, sourceAssets asset: Any!) {
        
    }
    
    func imagePickerController(_ picker: TZImagePickerController!, didFinishPickingGifImage animatedImage: UIImage!, sourceAssets asset: Any!) {
        
    }
    
    func imagePickerController(_ picker: TZImagePickerController!, didFinishPickingPhotos photos: [UIImage]!, sourceAssets assets: [Any]!, isSelectOriginalPhoto: Bool) {
        NSLog("[HOME] didFinishPickingPhotos %d", assets.count)
        
        
        if let assetArr = assets as? [PHAsset], AssetHelper.shared.lengthValid(assets: assetArr) {
            
            AssetHelper.shared.convertToAVAssets(from: assetArr, completionHandler: { [weak self] (avAssets) in
                guard let this = self else { return }
                
//                this.assets.append(contentsOf: avAssets)
                // Copy to Temporary folder
                
                DispatchQueue.global(qos: .background).async {
                    do {
                        let dispatchGroup = DispatchGroup.init()
                        dispatchGroup.enter()
                        
                        var completedAssetCount = 0
                        var completedURLs = [URL]()
                        
                        for index in 0..<avAssets.count {
                            let statusString = String.init(format: "Copying %d/%d file%@", index + 1, avAssets.count, avAssets.count > 1 ? "s" : "")
                            SVProgressHUD.showInfo(withStatus: statusString)
                            let sourceVideoUrl = (avAssets[index] as! AVURLAsset).url
                            let destinationVideoURL = URL.init(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true).appendingPathComponent(UUID.init().uuidString + "." + sourceVideoUrl.pathExtension)
                            try FileManager.default.copyItem(at: sourceVideoUrl, to: destinationVideoURL)
                            NSLog("Copied video to %@", destinationVideoURL.path)
                            
                            completedURLs.append(destinationVideoURL)
                            completedAssetCount += 1
                            if completedAssetCount == avAssets.count {
                                dispatchGroup.leave()
                            }
                        }
                        
                        dispatchGroup.notify(queue: .main, execute: {
                            SVProgressHUD.dismiss()
                            this.assets.append(contentsOf: completedURLs.map { AVURLAsset.init(url: $0)} )
                            this.prepare(this.stackVideosView, with: this.assets)
                            
                        })
                    } catch let error as NSError {
                        NSLog("Error on copying video asset: %@", error.debugDescription)
                    }
                }
                
                DispatchQueue.main.async {
                    this.prepare(this.stackVideosView, with: this.assets)
                }
            })
        } else {
            NSLog("Asset not validated")
        }
        
    }
    
    func imagePickerController(_ picker: TZImagePickerController!, didFinishPickingPhotos photos: [UIImage]!, sourceAssets assets: [Any]!, isSelectOriginalPhoto: Bool, infos: [[AnyHashable : Any]]!) {
        
    }
    
    func tz_imagePickerControllerDidCancel(_ picker: TZImagePickerController!) {
        
    }
}

