//
//  ExportViewController.swift
//  VideoEditor-CropMerger
//
//  Created by Bá Anh Nguyễn on 12/19/17.
//  Copyright © 2017 Bá Anh Nguyễn. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import Photos
import UICircularProgressRing

class ExportViewController: BaseViewController {

    @IBOutlet weak var segOutputFormat: UISegmentedControl!
    @IBOutlet weak var segQuality: UISegmentedControl!
    @IBOutlet weak var viewProgressContainer: UIView!
    @IBOutlet weak var btnStartStop: UIButton!
    @IBOutlet weak var svAction: UIStackView!
    
    var progressRing: UICircularProgressRingView!
    var progressTimer: Timer!
    
    var exporter: AVAssetExportSession!
    var asset: AVAsset!
    var outputURL: URL!
    
    fileprivate let outputFormatArray = [
        AVFileTypeQuickTimeMovie,
        AVFileTypeMPEG4,
        AVFileTypeAppleM4V
    ]
    fileprivate let outputFormatExtension = [
        ".mov",
        ".mp4",
        ".m4v"
    ]
    fileprivate let outputQualityArray = [
        AVAssetExportPresetLowQuality,
        AVAssetExportPresetMediumQuality,
        AVAssetExportPresetHighestQuality
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Export"
        
        btnStartStop.addTarget(self, action: #selector(btnStartStopHandler), for: .touchUpInside)
        
        progressRing = UICircularProgressRingView(frame: viewProgressContainer.bounds)
        progressRing.maxValue = 100
        progressRing.innerRingColor = UIColor.Palette.primary
        progressRing.innerCapStyle = .butt
        
        viewProgressContainer.layout(progressRing).edges()
        
        svAction.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareUI() {
        super.prepareUI()
    }

    
    func config(data: Any) { // use Any for a quick test
        self.asset = data as! AVAsset
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }

    func btnStartStopHandler() {
        
        if exporter == nil {
            processExport()
        } else if exporter.progress < 1.0 {
            exporter.cancelExport()
            btnStartStop.setTitle("START", for: .normal)
        }
        
    }
    
    @IBAction func btnPreviewDidPress(_ sender: Any) {
        guard let url = outputURL else { return }
        let playerVC = AVPlayerViewController.init()
        playerVC.player = AVPlayer.init(url: url)
        present(playerVC, animated: true, completion: nil)
    }
    
    @IBAction func btnSaveDidPress(_ sender: Any) {
        saveToLibrary()
    }
    
    func processExport() {
        svAction.isHidden = true
        outputURL = nil
        
        let dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = "yyyyMMdd'-'HHmmss"
        let outputFileName = "VideoExport_" + dateFormatter.string(from: Date.init()) + outputFormatExtension[segOutputFormat.selectedSegmentIndex]
        outputURL = URL.init(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(outputFileName)
        
        exporter = AVAssetExportSession(asset: asset, presetName: outputQualityArray[segQuality.selectedSegmentIndex])
        guard let exporter = exporter else { return }
        
        exporter.outputURL = outputURL
        exporter.outputFileType = outputFormatArray[segOutputFormat.selectedSegmentIndex]
        exporter.shouldOptimizeForNetworkUse = true
        
        //        exporter.videoComposition = videoComposition // 👈
        
        exporter.exportAsynchronously {
            self.exportDidFinish()
        }
        
        btnStartStop.setTitle("STOP", for: .normal)
        progressTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateProgress), userInfo: nil, repeats: true)
    }
    
    func updateProgress() {
        if progressRing != nil, exporter != nil {
            progressRing.setProgress(value: CGFloat(exporter.progress * 100), animationDuration: 0.1)
            if exporter.progress > 0.99 {
                progressTimer.invalidate()
                btnStartStop.setTitle("START", for: .normal)
            }
        }
    }
    
    func exportDidFinish() {
        guard let url = outputURL else { return }
        NSLog("Ouput URL %@", url.path)
        
        // show Action Menu
        DispatchQueue.main.async {
            self.svAction.isHidden = false
            self.progressRing.setProgress(value: 0.0, animationDuration: 0.1)
        }
        
    }
    
    func saveToLibrary() {
        guard let url = outputURL else { return }
        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)
        }) { (success, error) in
            DispatchQueue.main.async {
                if success {
                    UIAlertView.init(title: "Saved to library", message: nil, delegate: nil, cancelButtonTitle: "OK").show()
                } else {
                    UIAlertView.init(title: "Failed on saving to library", message: nil, delegate: nil, cancelButtonTitle: "OK").show()
                }
            }
            
        }
    }
}



