//
//  RotateVideoViewController.swift
//  VideoEditor
//
//  Created by Bá Anh Nguyễn on 10/30/17.
//  Copyright © 2017 Bá Anh Nguyễn. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import SVProgressHUD
import LLVideoEditor

protocol RotateVideoViewControllerDelegate {
    func didFinishRotate(_ viewController: RotateVideoViewController, sourceUrlArray: [URL], outputUrlArray: [URL])
    
    func didFinishRotate(_ viewController: RotateVideoViewController, sourceUrl: URL, outputUrl: URL)
}

class RotateVideoViewController: BaseViewController {
    
    var btnRotate: UIBarButtonItem!
    var btnDone: UIBarButtonItem!
    var btnDiscard: UIBarButtonItem!

    @IBOutlet weak var stackVideosView: UIStackView!
    @IBOutlet weak var vPlayer: UIView!
    
    var delegate: RotateVideoViewControllerDelegate?
    
    var sourceURLs = [URL]()
    var assets = [AVURLAsset]()
    var player: AVPlayer!
    var playerVC: AVPlayerViewController!
    
    var videoEditor: LLVideoEditor!
    var rotateDegree: LLRotateDegree! {
        didSet {
            btnDone.isEnabled = nil != rotateDegree
            if let degree = rotateDegree {
                let degreeString = ( degree == LLRotateDegree90 ? "90" : (degree == LLRotateDegree180 ? "180" : (degree == LLRotateDegree270) ? "270" : "0") ) + "°"
                btnRotate.title = "Degree: " + degreeString
            } else {
                btnRotate.title = "Degree: 0°"
            }
            
        }
    }
    var isRotated = false // check if one of the assets is rotated or not? Dismiss imeediately if isRotated == false
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Rotate"
        
        btnDone = UIBarButtonItem.init(title: "Apply", style: .done, target: self, action: #selector(btnDoneHandler))
        btnRotate = UIBarButtonItem.init(title: "Degree: 0°", style: .plain, target: self, action: #selector(btnRotateHandler))
        btnDiscard = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(btnDiscardHandler))
        
        navigationItem.rightBarButtonItem = btnDiscard
        navigationItem.leftBarButtonItems = [btnRotate, btnDone]
        
        prepare(stackVideosView, with: assets)
        preparePlayer()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    /// Dismiss a vc or a vc emmbed in a navigation controller
    func dismissVC() {
        if let nav = navigationController {
            nav.dismiss(animated: true, completion: nil)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func btnDiscardHandler() {
        
        if sourceURLs.count == 1, let url = self.sourceURLs.first, let asset = self.assets.first {
            delegate?.didFinishRotate(self, sourceUrl: url, outputUrl: asset.url)
        } else {
            delegate?.didFinishRotate(self, sourceUrlArray: self.sourceURLs, outputUrlArray: self.assets.map { $0.url })
        }
        
        self.dismissVC()
    }
    
    @objc func btnRotateHandler() {
        guard let degree = rotateDegree else {
            rotateDegree = LLRotateDegree90
            return
        }
        switch degree.rawValue {
        case LLRotateDegree90.rawValue:
            rotateDegree = LLRotateDegree180
            break
            
        case LLRotateDegree180.rawValue:
            rotateDegree = LLRotateDegree270
            break
            
        case LLRotateDegree270.rawValue:
            rotateDegree = nil
            break
            
        default:
            rotateDegree = LLRotateDegree90
            break
        }
        
    }
    
    @objc func btnDoneHandler() {
        guard nil != rotateDegree else { return }
        let processGroup = DispatchGroup()
        processGroup.enter()
        SVProgressHUD.show(withStatus: "Proccessing...")
        var leavingCount = assets.count
        for i in 0..<assets.count {
            let currentAssetURL = assets[i].url
            let outputURL = currentAssetURL.deletingLastPathComponent().appendingPathComponent(UUID().uuidString + "." + currentAssetURL.pathExtension)
            videoEditor = LLVideoEditor.init(videoURL: currentAssetURL)
            videoEditor.rotate(rotateDegree)
            videoEditor.export(to: outputURL, completionBlock: { [weak self] (session) in
                leavingCount -= 1
                if leavingCount <= 0 {
                    processGroup.leave()
                    SVProgressHUD.dismiss()
                }
                guard let session = session, let this = self else { return }
                
                switch (session.status) {
                case .completed:
                    NSLog("Complete")
                    // Unlink old url
                    try? FileManager.default.removeItem(at: currentAssetURL)
                    
                    // Replace new url to asset array
                    this.rotateDegree = nil
                    this.assets[i] = AVURLAsset.init(url: outputURL)
                    this.prepare(this.stackVideosView, with: this.assets)
                    this.preparePlayer()
                    this.isRotated = true
                    break
                    
                case .failed:
                    NSLog("Failed: %@", session.error.debugDescription)
                    break
                    
                case .cancelled:
                    NSLog("Canceled: %@", session.error.debugDescription)
                    break
                    
                default:
                    break
                }
            })
        }
    }
    
    func config(withUrlArray urls: [URL]) {
        sourceURLs = urls
        self.config(withAVAssets: urls.map { AVURLAsset.init(url: $0) })
    }
    
    private func config(withAVAssets assets: [AVURLAsset]) {
        self.assets = assets
    }

}

extension RotateVideoViewController {
    
    // MARK: - Preparation
    
    func prepare(_ stackView: UIStackView, with assetArray: [AVURLAsset]) {
        NSLog("prepare stackView with assetArray")
        for v in stackView.arrangedSubviews { // Clear stack
            v.removeFromSuperview()
        }
        for index in 0..<assetArray.count { // Add new subviews
            let v = VideoAssetView()
            v.config(assetURL: assetArray[index].url, order: index + 1, shouldShowDeleteButton: false)
            v.tag = index // IMPORTANT, use to find new index after reordering in `stackViewDidEndReordering`.
            v.widthAnchor.constraint(equalToConstant: stackVideosView.height).isActive = true
            v.delegate = self
            stackView.addArrangedSubview(v)
        }
    }
    
    
    func preparePlayer() {
        guard let firstItem = assets.first else {
            return
        }
        if nil != player {
            player = nil
        }
        let item = AVPlayerItem.init(asset: firstItem)
        player = AVPlayer.init()
        player.replaceCurrentItem(with: item)
        
        if nil != playerVC {
            playerVC.view.removeFromSuperview()
            playerVC.removeFromParentViewController()
            playerVC = nil
        }
        playerVC = AVPlayerViewController()
        playerVC.player = player
        playerVC.view.frame = CGRect(origin: .zero, size: vPlayer.frame.size)
        addChildViewController(playerVC)
        vPlayer.addSubview(playerVC.view)
    }
}

extension RotateVideoViewController: VideoAssetViewDelegate {
    
    
    func didTap(_ view: VideoAssetView, assetURL: URL) {
        let item = AVPlayerItem.init(url: assetURL)
        player.replaceCurrentItem(with: item)
        player.play()
        player.pause()
    }
    
    func didSelectDelete(_ view: VideoAssetView, assetURL: URL) {
//        let alert = UIAlertController(title: "Confirm to delete", message: "Do you want to delete the video from the queue?", preferredStyle: .alert)
//        alert.addAction(UIAlertAction.init(title: "Yes", style: .default, handler: { (_) in
//            let index = view.tag //IMPORTANT
//            self.assets.remove(at: index)
//            self.prepare(self.stackVideosView, with: self.assets)
//        }))
//        alert.addAction(UIAlertAction.init(title: "No", style: .cancel, handler: nil))
//        self.present(alert, animated: true, completion: nil)
    }
}

