//
//  ImportViewController.swift
//  VideoEditor-CropMerger
//
//  Created by Bá Anh Nguyễn on 12/13/17.
//  Copyright © 2017 Bá Anh Nguyễn. All rights reserved.
//

import UIKit
import MobileCoreServices
import TZImagePickerController
import SVProgressHUD
import AVFoundation

class ImportViewController: BaseViewController {

    @IBOutlet weak var btnImport: UIButton!
    @IBOutlet weak var btnCamera: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnImport.addTarget(self, action: #selector(btnImportHandler), for: .touchUpInside)
        btnCamera.addTarget(self, action: #selector(btnCameraHandler), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func prepareUI() {
        super.prepareUI()
        
        self.title = "Home"
        
        let normalTextAttributes = [
            NSFontAttributeName: UIFont.myFontLight(ofSize: 24),
            NSForegroundColorAttributeName: UIColor.Palette.primary
        ]
        
        btnImport.setAttributedTitle(NSAttributedString.init(string: "IMPORT VIDEO", attributes: normalTextAttributes), for: .normal)
        btnImport.layer.borderColor = UIColor.Palette.primary.cgColor
        btnImport.layer.borderWidth = 1.0
        
        
        btnCamera.setAttributedTitle(NSAttributedString.init(string: "CAMERA", attributes: normalTextAttributes), for: .normal)
        btnCamera.layer.borderColor = UIColor.Palette.primary.cgColor
        btnCamera.layer.borderWidth = 1.0
    }
    
    // MARK: Actions
    
    func btnImportHandler() {
//        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
////            let videoPicker = CTAssetsPickerController()
////            videoPicker.delegate = self
////            videoPicker.doneButtonTitle = "Import"
////            videoPicker.showsSelectionIndex = true
////            videoPicker.showsEmptyAlbums = false
////            videoPicker.showsCancelButton = true
////            videoPicker.defaultAssetCollection = PHAssetCollectionSubtype.smartAlbumVideos
//////            videoPicker.assetCollectionSubtypes = [PHAssetCollectionSubtype.smartAlbumVideos]
////
////            if UIDevice.current.userInterfaceIdiom == .pad {
////                videoPicker.modalPresentationStyle = .formSheet
////            }
////
//            let videoPicker = UIImagePickerController()
//            videoPicker.delegate = self
//            videoPicker.sourceType = .photoLibrary
//            videoPicker.mediaTypes = [kUTTypeMovie as String]
//            videoPicker.allowsEditing = false
//            self.present(videoPicker, animated: true, completion: nil)
//        } else {
//            print("photoLibrary not available.")
//        }
        
        if let imagePickerVC = TZImagePickerController.init(maxImagesCount: 9, columnNumber: 4, delegate: self) {
            imagePickerVC.allowPickingMultipleVideo = true
            imagePickerVC.allowPickingVideo     = true
            imagePickerVC.allowPickingImage     = false
            imagePickerVC.allowPickingGif       = false
            imagePickerVC.cancelBtnTitleStr     = "Cancel"
            imagePickerVC.doneBtnTitleStr       = "Done"
            imagePickerVC.previewBtnTitleStr    = "Preview"
            imagePickerVC.fullImageBtnTitleStr  = "Full Size"
            imagePickerVC.processHintStr        = "Loading"
            imagePickerVC.settingBtnTitleStr    = "Settings"
            self.present(imagePickerVC, animated: true, completion: nil)
        }
    }
    
    func btnCameraHandler() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let videoPicker = UIImagePickerController()
            videoPicker.delegate = self
            videoPicker.sourceType = .camera
            videoPicker.mediaTypes = [kUTTypeMovie as String]
            videoPicker.allowsEditing = true
            videoPicker.showsCameraControls = true
            
//            videoPicker = CTAssetsPickerController()
//            videoPicker.delegate = self
//            videoPicker.doneButtonTitle = "Import"
//            videoPicker.showsSelectionIndex = true
//            videoPicker.showsEmptyAlbums = false
//            videoPicker.showsCancelButton = true
//            if UIDevice.current.userInterfaceIdiom == .pad {
//                videoPicker.modalPresentationStyle = .formSheet
//            }
            
            self.present(videoPicker, animated: true, completion: nil)
        } else {
            print("camera not available.")
        }
    }
    
    
    func gotoEditor(with videoUrls: [URL]) {
        SVProgressHUD.showInfo(withStatus: "Loading Editor...")
        let vc = VideoEditorViewController()
        vc.configVideoURLs(videoUrls)
        self.navigationController?.pushViewController(vc, animated: true)
        SVProgressHUD.dismiss()
    }

}

extension ImportViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let sourceUrl = info[UIImagePickerControllerMediaURL] as? URL {
            self.dismiss(animated: true, completion: {
//                self.performSegue(withIdentifier: self.segueId, sender: videoUrl)
                
                //--- COPY AND Push
                // Copy to Temporary folder
                let destinationVideoURL = URL.init(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true).appendingPathComponent(UUID.init().uuidString + "." + sourceUrl.pathExtension)
                DispatchQueue.global(qos: .background).async {
                    do {
                        try FileManager.default.copyItem(at: sourceUrl, to: destinationVideoURL)
                        NSLog("Copied video to %@", destinationVideoURL.path)
                        
                        self.gotoEditor(with: [destinationVideoURL])
                        
                    } catch let error as NSError {
                        NSLog("Error on copying video asset: %@", error.debugDescription)
                    }
                }
                DispatchQueue.main.async {
                    let sourceFileSize = (try? FileManager.default.attributesOfItem(atPath: sourceUrl.path)[FileAttributeKey.size] as? Int64)
                    Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { (timer) in
                        let destinationFileSize = try? FileManager.default.attributesOfItem(atPath: destinationVideoURL.path)[FileAttributeKey.size] as? Int64
                        let progress = Float(Double(destinationFileSize!!) / Double(sourceFileSize!!)) // prevent divide 0
//                        self.progressView.setProgress(progress, animated: true)
                        NSLog("Copying progress: %@", progress.description)
                        if progress >= 1 {
                            timer.invalidate()
                        }
                    })
                }
                
                // ---
            })
        }
    }
}

extension ImportViewController: TZImagePickerControllerDelegate {
    
    
    func imagePickerController(_ picker: TZImagePickerController!, didFinishPickingVideo coverImage: UIImage!, sourceAssets asset: Any!) {
        
    }
    
    func imagePickerController(_ picker: TZImagePickerController!, didFinishPickingGifImage animatedImage: UIImage!, sourceAssets asset: Any!) {
        
    }
    
    func imagePickerController(_ picker: TZImagePickerController!, didFinishPickingPhotos photos: [UIImage]!, sourceAssets assets: [Any]!, isSelectOriginalPhoto: Bool) {
        NSLog("[HOME] didFinishPickingPhotos %d", assets.count)
        
        
        if let assetArr = assets as? [PHAsset], AssetHelper.shared.lengthValid(assets: assetArr) {
            let dispatchGroup = DispatchGroup()
            dispatchGroup.enter()
            var completedAssetCount = 0
            var maxAssetCount = assetArr.count
            var completedURLs = [URL]()
            assetArr.forEach({ (asset) in
//                DispatchQueue.global(qos: .background).async {
                
                    PHImageManager.init().requestAVAsset(forVideo: asset, options: nil, resultHandler: { (avAsset, audioMix, info) in
                        if let urlAsset = avAsset as? AVURLAsset {
                            do {
                                let sourceVideoUrl = urlAsset.url
                                NSLog("sourceVideoUrl = %@", sourceVideoUrl.path)
                                let statusString = String.init(format: "Copying %d/%d file%@", completedAssetCount + 1, maxAssetCount, maxAssetCount > 1 ? "s" : "")
                                SVProgressHUD.showInfo(withStatus: statusString)
                                let destinationVideoURL = URL.init(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true).appendingPathComponent(UUID.init().uuidString + "." + sourceVideoUrl.pathExtension)
                                try FileManager.default.copyItem(at: sourceVideoUrl, to: destinationVideoURL)
                                NSLog("Copied video to %@", destinationVideoURL.path)
                                completedURLs.append(destinationVideoURL)
                                completedAssetCount += 1
                                if completedAssetCount >= maxAssetCount {
                                    dispatchGroup.leave()
                                }
                            } catch let error as NSError {
                                NSLog("Error on copying video asset: %@", error.debugDescription)
                                completedAssetCount += 1
                                
                            } // .do-catch
                        } // .if
                        else {
                            NSLog("Not an AVAsset")
                            SVProgressHUD.showError(withStatus: "Skipped an asset!")
                            completedAssetCount += 1
//                            SVProgressHUD.showError(withStatus: "Cannot pick current video.")
                        }
                        
                    })
                    
                    
//                    asset.requestContentEditingInput(with: .init(), completionHandler: { (input, info) in
//                        if let urlAsset = input?.audiovisualAsset as? AVURLAsset {
//                            do {
//                                let sourceVideoUrl = urlAsset.url
//                                NSLog("sourceVideoUrl = %@", sourceVideoUrl.path)
//                                let statusString = String.init(format: "Copying %d/%d file%@", completedAssetCount + 1, assetArr.count, assetArr.count > 1 ? "s" : "")
//                                SVProgressHUD.showInfo(withStatus: statusString)
//                                let destinationVideoURL = URL.init(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true).appendingPathComponent(UUID.init().uuidString + "." + sourceVideoUrl.pathExtension)
//                                try FileManager.default.copyItem(at: sourceVideoUrl, to: destinationVideoURL)
//                                NSLog("Copied video to %@", destinationVideoURL.path)
//                                completedAssetCount += 1
//                                completedURLs.append(destinationVideoURL)
//                                if completedAssetCount == assetArr.count {
//                                    dispatchGroup.leave()
//                                }
//                            } catch let error as NSError {
//                                NSLog("Error on copying video asset: %@", error.debugDescription)
//                            } // .do-catch
//                        } // .if
//                    }) //.request
                    
//                } // .async
                
            }) // .for
            
            dispatchGroup.notify(queue: .main, execute: {
                SVProgressHUD.dismiss()
                self.gotoEditor(with: completedURLs)
            })
            
//                AssetHelper.shared.convertToAVAssets(from: assetArr, completionHandler: { [weak self] (avAssets) in
////                    AssetHelper.shared.fetchURLs(from: assetArr, completionHandler: { [weak self] (urlArray) in
//
//                    guard let this = self else { return }
//
//                    //                this.assets.append(contentsOf: avAssets)
//                    // Copy to Temporary folder
//                    DispatchQueue.global(qos: .background).async {
//                    do {
//                        let dispatchGroup = DispatchGroup()
//                        dispatchGroup.enter()
//
//                        var completedAssetCount = 0
//                        var completedURLs = [URL]()
//
//                        for index in 0..<avAssets.count {
//                            let statusString = String.init(format: "Copying %d/%d file%@", index + 1, avAssets.count, avAssets.count > 1 ? "s" : "")
//                            SVProgressHUD.showInfo(withStatus: statusString)
//                            let sourceVideoUrl = (avAssets[index] as! AVURLAsset).url
//                            let destinationVideoURL = URL.init(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true).appendingPathComponent(UUID.init().uuidString + "." + sourceVideoUrl.pathExtension)
//                            try FileManager.default.copyItem(at: sourceVideoUrl, to: destinationVideoURL)
//                            NSLog("Copied video to %@", destinationVideoURL.path)
//
//                            completedURLs.append(destinationVideoURL)
//                            completedAssetCount += 1
//                            if completedAssetCount == avAssets.count {
//                                dispatchGroup.leave()
//                            }
//                        }
//
//                        dispatchGroup.notify(queue: .main, execute: {
//                            SVProgressHUD.dismiss()
//                            this.gotoEditor(with: completedURLs)
//
//                        })
//                    } catch let error as NSError {
//                        NSLog("Error on copying video asset: %@", error.debugDescription)
//                    }
//                }
//
//            })
        } else {
            NSLog("Asset not validated")
        }
        
    }
    
    func imagePickerController(_ picker: TZImagePickerController!, didFinishPickingPhotos photos: [UIImage]!, sourceAssets assets: [Any]!, isSelectOriginalPhoto: Bool, infos: [[AnyHashable : Any]]!) {
        
    }
    
    func tz_imagePickerControllerDidCancel(_ picker: TZImagePickerController!) {
        
    }
}

