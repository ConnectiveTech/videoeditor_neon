//
//  BaseNavigationController.swift
//  ContactBackup
//
//  Created by Bá Anh Nguyễn on 12/4/17.
//  Copyright © 2017 Bá Anh Nguyễn. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

class BaseNavigationController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return UIStatusBarStyle.default
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
    
    func prepareUI() {
        let textAttributes = [
            NSFontAttributeName: UIFont.myFontLight(ofSize: 14)
        ]
        
        UIBarButtonItem.appearance().setTitleTextAttributes(textAttributes, for: .application)
        UIBarButtonItem.appearance().setTitleTextAttributes(textAttributes, for: .normal)
        UIBarButtonItem.appearance().setTitleTextAttributes(textAttributes, for: .highlighted)
        UIBarButtonItem.appearance().setTitleTextAttributes(textAttributes, for: .disabled)
        
        navigationBar.isTranslucent         = false
        navigationBar.barTintColor          = UIColor.Palette.background
        navigationBar.backgroundColor       = navigationBar.barTintColor
        navigationBar.tintColor             = UIColor.Palette.primary
        navigationBar.titleTextAttributes   = [
            NSForegroundColorAttributeName: UIColor.Palette.title,
            NSFontAttributeName: UIFont.myFontLight(ofSize: 16)
        ]
        
        navigationBar.layer.shadowColor     = UIColor.Palette.defaultShadow.cgColor
        navigationBar.layer.shadowOffset    = .init(width: 0, height: 4)
        navigationBar.layer.shadowOpacity   = 1.0
        navigationBar.layer.shadowRadius    = 2.0
        
        if let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView {
            statusBar.backgroundColor       = navigationBar.barTintColor
            statusBar.tintColor             = navigationBar.tintColor
        }
    }

}
