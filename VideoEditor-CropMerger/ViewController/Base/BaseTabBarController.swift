//
//  BaseTabBarController.swift
//  ContactBackup
//
//  Created by Bá Anh Nguyễn on 12/5/17.
//  Copyright © 2017 Bá Anh Nguyễn. All rights reserved.
//

import UIKit

class BaseTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.barTintColor    = UIColor.Palette.primary
        self.tabBar.tintColor       = UIColor.Palette.background
        self.tabBar.unselectedItemTintColor = self.tabBar.tintColor.withAlphaComponent(0.5)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont.myFontRegular(ofSize: 10)], for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
