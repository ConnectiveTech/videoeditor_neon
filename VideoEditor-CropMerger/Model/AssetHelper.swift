//
//  AssetHelper.swift
//  VideoEditor
//
//  Created by Bá Anh Nguyễn on 10/11/17.
//  Copyright © 2017 Bá Anh Nguyễn. All rights reserved.
//

import Foundation
import Photos
import AVFoundation

class AssetHelper {
    static let shared = AssetHelper()
    private init() {}
    static let MIN_LENGTH: Int64 = 3000
    static let MAX_LENGTH: Int64 = 600000
}

protocol AssetHelperProtocol {
    /// return disk space size in bytes
    func size(_ asset: PHAsset) -> Int64
    
    /// return length in milisecond
    func length(_ videoAsset: PHAsset) -> Int64
    func length(_ videoAsset: AVAsset) -> Int64
    func length(videoAssets: [AVAsset]) -> Int64
    
    /// check video length in range is valid or not, default: 3s ~ 10min
    func lengthValid(assets: [PHAsset], withMinLength minLength: Int64, maxLength: Int64) -> Bool
    
    /// Convert an array from PHAsset to AVAsset
    func convertToAVAssets(from phAssets: [PHAsset], completionHandler: @escaping ([AVAsset])->())
    
    /// Export preview clips from an asset
    func exportClips(fromAsset asset: AVAsset, withSize size: CGSize, miliSecondsInterval timeInterval: Int, startFirstClipAt timeIntervalOffset: Int) -> [UIImage]
    
    /// Get AssetURL
    func fetchURLs(from phAssets: [PHAsset], completionHandler: @escaping ([URL])->())
}

extension AssetHelper: AssetHelperProtocol {
    func fetchURLs(from phAssets: [PHAsset], completionHandler: @escaping ([URL])->()) {
        let returnURLs = NSMutableArray()
        var countFinish = 0
        let countMax = phAssets.count
        let convertGroup = DispatchGroup()
        convertGroup.enter()
        for a in phAssets {
            if a.mediaType == .video {
                a.requestContentEditingInput(with: .init(), completionHandler: { (input, info) in
                    if let avUrlAsset = input?.avAsset as? AVURLAsset {
                        returnURLs.add(avUrlAsset.url)
                    }
                    countFinish += 1
                    if countFinish >= countMax {
                        convertGroup.leave()
                    }
                })
            }
        }
        convertGroup.wait()
        convertGroup.notify(queue: .global()) {
            completionHandler(Array(returnURLs) as! [URL])
        }
    }
    
    /// Export preview clips from an asset
    func exportClips(fromAsset asset: AVAsset, withSize size: CGSize, miliSecondsInterval timeInterval: Int, startFirstClipAt timeIntervalOffset: Int = 0) -> [UIImage] {
        let previewSize = CGSize.init(width: size.width * 2, height: size.height * 2) // 2x == smoother
        var images = [UIImage]()
        
        // Prepare Clips
        let originalDuration = CMTimeGetSeconds(asset.duration) * 1000
        let totalDuration = originalDuration - Double(timeIntervalOffset)
        var imageGenerator: AVAssetImageGenerator!
        let count = originalDuration.truncatingRemainder(dividingBy: Double(timeInterval)) < Double(timeInterval) / 2.0 ? floor(totalDuration / Double(timeInterval)) : ceil(totalDuration / Double(timeInterval))
        let clipCount = NSNumber.init(value: count).intValue
        for i in 0..<clipCount {
            let relativeTime = i * timeInterval + timeIntervalOffset
            imageGenerator = AVAssetImageGenerator(asset: asset)
            imageGenerator.maximumSize = previewSize
            let absoluteTime = CMTime.init(value: CMTimeValue(Double(relativeTime)), timescale: 1000)
            if let imageRef = try? imageGenerator.copyCGImage(at: absoluteTime, actualTime: nil) {
                let image = UIImage.init(cgImage: imageRef)
                images.append(image)
            }
        }
        return images
    }
    
    /// Convert an arrafrom PHAsset to AVAsset
    func convertToAVAssets(from phAssets: [PHAsset], completionHandler: @escaping ([AVAsset]) -> ()) {
        let returnAssets = NSMutableArray()
        var countFinish = 0
        let countMax = phAssets.count
        
        let convertGroup = DispatchGroup()
        convertGroup.enter()
        for a in phAssets {
            if a.mediaType == .video {
                
                PHImageManager.init().requestAVAsset(forVideo: a, options: nil, resultHandler: { (asset, _, _) in
                    if let asset = asset {
                        returnAssets.add(asset)
                    }
                    countFinish += 1
                    if countFinish >= countMax {
                        convertGroup.leave()
                    }
                })
            }
        }
        convertGroup.wait()
        convertGroup.notify(queue: .global()) {
            completionHandler(Array(returnAssets) as! [AVAsset])
        }
    }


    
    func lengthValid(assets: [PHAsset], withMinLength minLength: Int64 = MIN_LENGTH, maxLength: Int64 = MAX_LENGTH) -> Bool {
        for a in assets {
            if AssetHelper.shared.length(a) < minLength || AssetHelper.shared.length(a) > maxLength {
                return false
            }
        }
        return true
    }
    
    func lengthValid(assets: [AVAsset], withMinLength minLength: Int64 = MIN_LENGTH, maxLength: Int64 = MAX_LENGTH) -> Bool {
        for a in assets {
            if AssetHelper.shared.length(a) < minLength || AssetHelper.shared.length(a) > maxLength {
                return false
            }
        }
        return true
    }
    
    func length(_ videoAsset: AVAsset) -> Int64 {
        let time = CMTimeGetSeconds(videoAsset.duration) * 1000
        return NSNumber.init(value: time).int64Value
    }

    
    func length(_ videoAsset: PHAsset) -> Int64 {
        if videoAsset.mediaType == .video {
            let time = videoAsset.duration * 1000
            return NSNumber.init(value: time).int64Value
        }
        return 0
    }
    
    func length(videoAssets: [AVAsset]) -> Int64 {
        var duration:Int64 = 0
        for a in videoAssets {
            duration += self.length(a)
        }
        return duration
    }

    
    func size(_ asset: PHAsset) -> Int64 {
        if let resource = PHAssetResource.assetResources(for: asset).first {
            return Int64(bitPattern: UInt64(resource.value(forKey: "fileSize") as! CLong))
        }
        return 0
    }

    
}
