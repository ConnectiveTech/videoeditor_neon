//
//  ImportMenuView.swift
//  VideoEditor-CropMerger
//
//  Created by Bá Anh Nguyễn on 12/13/17.
//  Copyright © 2017 Bá Anh Nguyễn. All rights reserved.
//

import UIKit
import Material

protocol ImportMenuViewDelegate {
    func menuViewDidPressLibraryButton(_ menu: ImportMenuView)
    func menuViewDidPressCameraButton(_ menu: ImportMenuView)
    func menuViewDidPressArrangeButton(_ menu: ImportMenuView)
}

class ImportMenuView: UIView {
    @IBOutlet weak var btnLibrary: UIButton!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnArrange: UIButton!
    
    var delegate: ImportMenuViewDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fromNib()
        configUI()
    }
    
    override init(frame: CGRect = CGRect.init(origin: .zero, size: CGSize.init(width: UIScreen.main.bounds.width, height: 80))) {
        super.init(frame: frame)
        guard let contentView = Bundle.main.loadNibNamed("\(ImportMenuView.self)", owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.backgroundColor = .clear
        self.layout(contentView).top().right().left().bottom()
        configUI()
    }
    
    private func configUI() {
        self.backgroundColor = .clear
        
        let normalTextAttributes = [
            NSFontAttributeName: UIFont.myFontLight(ofSize: 16),
            NSForegroundColorAttributeName: UIColor.Palette.primary
        ]
        
        btnLibrary.setAttributedTitle(NSAttributedString.init(string: "IMPORT", attributes: normalTextAttributes), for: .normal)
        btnLibrary.layer.borderColor = UIColor.Palette.primary.cgColor
        btnLibrary.layer.borderWidth = 1.0
        
        
        btnCamera.setAttributedTitle(NSAttributedString.init(string: "CAMERA", attributes: normalTextAttributes), for: .normal)
        btnCamera.layer.borderColor = UIColor.Palette.primary.cgColor
        btnCamera.layer.borderWidth = 1.0
        
        btnArrange.setAttributedTitle(NSAttributedString.init(string: "ARRANGE", attributes: normalTextAttributes), for: .normal)
        btnArrange.layer.borderColor = UIColor.Palette.primary.cgColor
        btnArrange.layer.borderWidth = 1.0
    }

    @IBAction func btnLibraryDidPress(_ sender: Any) {
        delegate?.menuViewDidPressLibraryButton(self)
    }
    
    @IBAction func btnCameraDidPress(_ sender: Any) {
        delegate?.menuViewDidPressCameraButton(self)
    }
    
    @IBAction func btnArrangeDidPress(_ sender: Any) {
        delegate?.menuViewDidPressArrangeButton(self)
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
