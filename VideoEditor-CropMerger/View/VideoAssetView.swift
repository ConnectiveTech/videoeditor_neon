//
//  VideoAssetView.swift
//  VideoEditor
//
//  Created by Bá Anh Nguyễn on 10/26/17.
//  Copyright © 2017 Bá Anh Nguyễn. All rights reserved.
//

import UIKit
import Material
import AVFoundation


protocol VideoAssetViewDelegate {
    func didTap(_ view: VideoAssetView, assetURL: URL)
    func didSelectDelete(_ view: VideoAssetView, assetURL: URL)
}

class VideoAssetView: UIView {
    
    @IBOutlet weak var ivThumbnail: UIImageView!
    @IBOutlet weak var lbDuration: UILabel!
    @IBOutlet weak var lbOrder: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var delegate: VideoAssetViewDelegate?
    
    var asset: AVURLAsset!
    
    required init?(coder aDecoder: NSCoder) {   // 2 - storyboard initializer
        super.init(coder: aDecoder)
        fromNib()   // 5.
    }
    
    init() {   // 3 - programmatic initializer
        super.init(frame: CGRect.zero)  // 4.
        guard let contentView = Bundle.main.loadNibNamed("\(VideoAssetView.self)", owner: self, options: nil)?.first as? UIView else {    // 3
            return // xib not loaded, or its top view is of the wrong type
        }
        self.addSubview(contentView)     // 4
        contentView.translatesAutoresizingMaskIntoConstraints = false   // 5
        self.layout(contentView).top().right().left().bottom()
        btnDelete.addTarget(self, action: #selector(btnDeletePressed), for: .touchUpInside)
    }

//    override func awakeFromNib() {
//        super.awakeFromNib()
////        btnDelete.isHidden = true
//        btnDelete.addTarget(self, action: #selector(btnDeletePressed), for: .touchUpInside)
//    }
    
    @objc func btnDeletePressed() {
        delegate?.didSelectDelete(self, assetURL: asset.url)
    }
    
    func config(assetURL: URL, order: Int? = nil, shouldShowDeleteButton: Bool = true) {
        self.config(with: AVURLAsset.init(url: assetURL), order: order, shouldShowDeleteButton: shouldShowDeleteButton)
    }
    
    private func config(with asset: AVURLAsset, order: Int? = nil, shouldShowDeleteButton: Bool = true) {
        self.asset = asset
        let imageGenerator = AVAssetImageGenerator.init(asset: asset)
        imageGenerator.maximumSize = CGSize(width: frame.size.width * 2.0, height: frame.size.height * 2.0)
        if let imageRef = try? imageGenerator.copyCGImage(at: kCMTimeZero, actualTime: nil) {
            let image = UIImage.init(cgImage: imageRef)
            ivThumbnail.image = image
        }
        self.btnDelete.isHidden = !shouldShowDeleteButton
        lbDuration.text = Util.populateTime(with: CMTimeGetSeconds(asset.duration))
        if let order = order {
            lbOrder.text = "\(order)"
        } else {
            lbOrder.isHidden = true
        }
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(tapHandler))
        self.addGestureRecognizer(tap)
        
        lbOrder.layer.cornerRadius = lbOrder.frame.width / 2.0
    }
    
    func setSelected(_ selected: Bool = false) {
        if selected {
            lbOrder.textColor = UIColor.Palette.primary
        } else {
            lbOrder.textColor = UIColor.Palette.header
        }
    }
    
    @objc func tapHandler() {
        NSLog("tapHandler")
        delegate?.didTap(self, assetURL: asset.url)
    }
    
}
