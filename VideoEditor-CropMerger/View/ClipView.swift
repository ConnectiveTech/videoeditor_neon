//
//  ClipView.swift
//  VideoEditor
//
//  Created by Bá Anh Nguyễn on 10/30/17.
//  Copyright © 2017 Bá Anh Nguyễn. All rights reserved.
//

import UIKit
import Material
import AVFoundation

class ClipView: UIView {
    
    @IBOutlet weak var ivThumbnail: UIImageView!
    @IBOutlet weak var lbTime: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fromNib()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        guard let contentView = Bundle.main.loadNibNamed("\(ClipView.self)", owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        self.layout(contentView).top().right().left().bottom()
    }
    
    func config(with image: UIImage, at second: Double) {
        ivThumbnail.image = image
        lbTime.text = Util.populateTime(with: second)
    }
    
    func config(asset: AVAsset, at secound: Double) {
        let imageGenerator = AVAssetImageGenerator.init(asset: asset)
        var maximumSize = CGSize(width: frame.size.width * 2.0, height: frame.size.height * 2.0)
        if let size = asset.tracks(withMediaType: AVMediaTypeVideo).first?.naturalSize {
            let edge = size.width > size.height ? size.width : size.height
            let ratio = edge / self.frame.size.width / 2
            maximumSize = CGSize(width: size.width / ratio, height: size.height / ratio)
        }
        imageGenerator.maximumSize = maximumSize
        if let imageRef = try? imageGenerator.copyCGImage(at: kCMTimeZero, actualTime: nil) {
            let image = UIImage.init(cgImage: imageRef)
            ivThumbnail.image = image
        }
        
        lbTime.text = Util.populateTime(with: secound)
    }
    
}
