//
//  VideoToolMenuView.swift
//  VideoEditor-CropMerger
//
//  Created by Bá Anh Nguyễn on 12/13/17.
//  Copyright © 2017 Bá Anh Nguyễn. All rights reserved.
//

import UIKit
import Material

protocol VideoToolMenuViewDelegate {
    func menuViewDidPressClipButton(_ menu: VideoToolMenuView, for url: URL?)
//    func menuViewDidPressSpeedButton(_ menu: VideoToolMenuView, for url: URL?)
//    func menuViewDidPressVolumeButton(_ menu: VideoToolMenuView, for url: URL?)
    func menuViewDidPressRotateButton(_ menu: VideoToolMenuView, for url: URL?)
}

class VideoToolMenuView: UIView {
    @IBOutlet weak var btnClip: UIButton!
//    @IBOutlet weak var btnSpeed: UIButton!
//    @IBOutlet weak var btnVolume: UIButton!
    @IBOutlet weak var btnRotate: UIButton!
    
    var delegate: VideoToolMenuViewDelegate?
    
    var sendingURL: URL?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fromNib()
        configUI()
    }
    
    override init(frame: CGRect = CGRect.init(origin: .zero, size: CGSize.init(width: UIScreen.main.bounds.width, height: 80))) {
        super.init(frame: frame)
        guard let contentView = Bundle.main.loadNibNamed("\(VideoToolMenuView.self)", owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.backgroundColor = .clear
        self.layout(contentView).top().right().left().bottom()
        configUI()
    }
    
    private func configUI() {
        self.backgroundColor = .clear
        
        let normalTextAttributes = [
            NSFontAttributeName: UIFont.myFontLight(ofSize: 16),
            NSForegroundColorAttributeName: UIColor.Palette.primary
        ]
        
        btnClip.setAttributedTitle(NSAttributedString.init(string: "TRIM", attributes: normalTextAttributes), for: .normal)
        btnClip.layer.borderColor = UIColor.Palette.primary.cgColor
        btnClip.layer.borderWidth = 1.0
        
//        btnSpeed.setAttributedTitle(NSAttributedString.init(string: "SPEED", attributes: normalTextAttributes), for: .normal)
//        btnSpeed.layer.borderColor = UIColor.Palette.primary.cgColor
//        btnSpeed.layer.borderWidth = 1.0
//
//        btnVolume.setAttributedTitle(NSAttributedString.init(string: "VOLUME", attributes: normalTextAttributes), for: .normal)
//        btnVolume.layer.borderColor = UIColor.Palette.primary.cgColor
//        btnVolume.layer.borderWidth = 1.0
        
        btnRotate.setAttributedTitle(NSAttributedString.init(string: "ROTATE", attributes: normalTextAttributes), for: .normal)
        btnRotate.layer.borderColor = UIColor.Palette.primary.cgColor
        btnRotate.layer.borderWidth = 1.0
        
        buttonHandlers()
    }
    
    func buttonHandlers() {
        btnClip.addTarget(self, action: #selector(btnClipHandler), for: .touchUpInside)
//        btnSpeed.addTarget(self, action: #selector(btnSpeedHandler), for: .touchUpInside)
//        btnVolume.addTarget(self, action: #selector(btnVolumeHandler), for: .touchUpInside)
        
        btnRotate.addTarget(self, action: #selector(btnRotateHandler), for: .touchUpInside)
    }
    
    func btnClipHandler() {
        delegate?.menuViewDidPressClipButton(self, for: sendingURL)
    }
    
//    func btnSpeedHandler() {
//        delegate?.menuViewDidPressSpeedButton(self, for: sendingURL)
//    }
//    
//    func btnVolumeHandler() {
//        delegate?.menuViewDidPressVolumeButton(self, for: sendingURL)
//    }
    
    func btnRotateHandler() {
        delegate?.menuViewDidPressRotateButton(self, for: sendingURL)
    }

}
