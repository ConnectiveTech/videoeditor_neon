//
//  ClipCollectionViewCell.swift
//  VideoEditor
//
//  Created by Bá Anh Nguyễn on 11/9/17.
//  Copyright © 2017 Bá Anh Nguyễn. All rights reserved.
//

import UIKit

class ClipCollectionViewCell: UICollectionViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init() {
        super.init(frame: CGRect.zero)
        guard let contentView = Bundle.main.loadNibNamed("\(ClipCollectionViewCell.self)", owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        self.layout(contentView).top().right().left().bottom()
    }
    

}
