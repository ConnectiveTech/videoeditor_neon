//
//  Util.swift
//  VideoEditor
//
//  Created by Bá Anh Nguyễn on 10/23/17.
//  Copyright © 2017 Bá Anh Nguyễn. All rights reserved.
//

import Foundation
import AVFoundation

class Util {
    static func populateTime(with time: Double, decimalSecond: Bool = false) -> String {
        if time <= 0 {
            return "00:00"
        }
        let minutes = Int(time / 60)
        if decimalSecond {
            let seconds = time - Double(minutes * 60)
            return String(format: "%02d:%02.2f", minutes, seconds)
        } else {
            let seconds = Int(time) - minutes * 60
            return String(format: "%02d:%02d", minutes, seconds)
        }
        
    }
    
    static func computeTimeString(with timeString: String) -> Double? {
        var hour = 0, min = 0
        var sec = 0.0
        
        let components = timeString.components(separatedBy: ":")
        if components.count == 2, let m = Int(components[0]), let s = Double(components[1]) {
                min = m
                sec = s
        } else if components.count == 3, let h = Int(components[0]), let m = Int(components[1]), let s = Double(components[2]) {
                hour = h
                min = m
                sec = s
        } else {
            return nil
        }
        return Double(hour * 3600 + min * 60) + sec
    }
    
    static func fileType(from fileExtension: String) -> String {
        let ext = fileExtension.lowercased()
        switch ext {
        case "mp4":
            return AVFileTypeMPEG4
            
        case "m4v":
            return AVFileTypeAppleM4V
            
        case "mov":
            return AVFileTypeQuickTimeMovie
            
        case "3gp":
            fallthrough
        case "3gpp":
            return AVFileType3GPP
            
        case "3g2":
            fallthrough
        case "3gp2":
            return AVFileType3GPP2
            
        default:
            return AVFileTypeMPEGLayer3
        }
    }
}
