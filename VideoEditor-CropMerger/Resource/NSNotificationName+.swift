//
//  NSNotificationName+Extension.swift
//  RemoveMaster
//
//  Created by Bá Anh Nguyễn on 7/17/17.
//  Copyright © 2017 Bá Anh Nguyễn. All rights reserved.
//

import Foundation

extension NSNotification.Name {
    struct AppPhoto {
        public static let DidSelectAll      = Notification.Name(rawValue: "app_photo_view_did_select_all")
        public static let DidDeselectAll    = Notification.Name(rawValue: "app_photo_view_did_deselect_all")
        public static let DidSelect         = Notification.Name(rawValue: "app_photo_view_did_select")
        public static let DidDeselect       = Notification.Name(rawValue: "app_photo_view_did_deselect")
        
        public static let FetchingDataProgress      = Notification.Name(rawValue: "app_photo_service_fetching_data_progress")
        public static let StopFetchingData          = Notification.Name(rawValue: "app_photo_view_ask_to_stop_data_progress")
        public static let FindingDuplicatesProgress = Notification.Name(rawValue: "app_photo_service_finding_duplicates_progress")
        
        public static let DidFinishSegment = Notification.Name(rawValue: "app_photo_service_did_finish_segment")
    }
    
    struct AppContact {
        public static let DidSelectAll      = Notification.Name(rawValue: "app_contact_view_did_select_all")
        public static let DidDeselectAll    = Notification.Name(rawValue: "app_contact_view_did_deselect_all")
        public static let DidSelect         = Notification.Name(rawValue: "app_contact_view_did_select")
        public static let DidDeselect       = Notification.Name(rawValue: "app_contact_view_did_deselect")
        public static let SectionSelect     = Notification.Name(rawValue: "app_contact_view_section_select")
        public static let SectionDeselect     = Notification.Name(rawValue: "app_contact_view_section_deselect")
        
        public static let FetchingDataProgress      = Notification.Name(rawValue: "app_contact_service_fetching_data_progress")
        public static let StopFetchingData          = Notification.Name(rawValue: "app_contact_view_ask_to_stop_data_progress")
        public static let FindingDuplicatesProgress = Notification.Name(rawValue: "app_contact_service_finding_duplicates_progress")
    }
}
