//
//  Bool+.swift
//  ContactBackup
//
//  Created by Bá Anh Nguyễn on 12/1/17.
//  Copyright © 2017 Bá Anh Nguyễn. All rights reserved.
//

import Foundation


extension Bool {
    
    // Toggle value from True -> False, or False -> True
    mutating func toggle() {
        self = !self
    }
}
