//
//  UIView+.swift
//  ContactBackup
//
//  Created by Bá Anh Nguyễn on 11/29/17.
//  Copyright © 2017 Bá Anh Nguyễn. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    @discardableResult   // 1
    func fromNib<T : UIView>() -> T? {   // 2
        guard let contentView = Bundle.main.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)?.first as? T else {    // 3
            // xib not loaded, or its top view is of the wrong type
            return nil
        }
        self.addSubview(contentView)     // 4
        contentView.translatesAutoresizingMaskIntoConstraints = false   // 5
        //        contentView.layoutAttachAll(to: self)   // 6
//        self.layout(contentView).top().right().left().bottom()
        contentView.layoutMargins = .init(top: 0, left: 0, bottom: 0, right: 0)
        return contentView   // 7
    }
    
    
    class func fromNib<T : UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
