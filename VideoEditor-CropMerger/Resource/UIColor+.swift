//
//  UIColor+.swift
//  ContactBackup
//
//  Created by Bá Anh Nguyễn on 12/4/17.
//  Copyright © 2017 Bá Anh Nguyễn. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    public convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    public convenience init(hex: Int, alpha: CGFloat = 1) {
        let divisor = CGFloat(255)
        let red     = CGFloat((hex & 0xFF0000) >> 16) / divisor
        let green   = CGFloat((hex & 0x00FF00) >>  8) / divisor
        let blue    = CGFloat( hex & 0x0000FF       ) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
        
    }
    
    struct Palette {
        static let primary = UIColor.init(hex: 0x11d2d1)
        static let background = UIColor.init(hex: 0x25262b)
        static let defaultShadow = UIColor.init(hex: 0x000000, alpha: 0.12)
        
        static let header = UIColor.init(hex: 0xFFFFFF)
        static let title = UIColor.init(hex: 0xEEEEEE)
        static let detail = UIColor.init(hex: 0x888888) // Medium Gray
        static let lightDetail = UIColor.init(hex: 0xCEB7FF) // Light Purple
        static let shadow = UIColor.init(hex: 0x5935A7) // Dark Purple
        
    }
    
}
