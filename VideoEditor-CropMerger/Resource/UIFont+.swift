//
//  UIFont+.swift
//  ContactBackup
//
//  Created by Bá Anh Nguyễn on 12/4/17.
//  Copyright © 2017 Bá Anh Nguyễn. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    class func myFontRegular(ofSize size: CGFloat) -> UIFont {
        return UIFont.init(name: "Montserrat-Regular", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    class func myFontMedium(ofSize size: CGFloat) -> UIFont {
        return UIFont.init(name: "Montserrat-Medium", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    class func myFontSemiBold(ofSize size: CGFloat) -> UIFont {
        return UIFont.init(name: "Montserrat-SemiBold", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    class func myFontBold(ofSize size: CGFloat) -> UIFont {
        return UIFont.init(name: "Montserrat-Bold", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    class func myFontExtraBold(ofSize size: CGFloat) -> UIFont {
        return UIFont.init(name: "Montserrat-ExtraBold", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    class func myFontBlack(ofSize size: CGFloat) -> UIFont {
        return UIFont.init(name: "Montserrat-Black", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    class func myFontLight(ofSize size: CGFloat) -> UIFont {
        return UIFont.init(name: "Montserrat-Light", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    class func myFontExtraLight(ofSize size: CGFloat) -> UIFont {
        return UIFont.init(name: "Montserrat-ExtraLight", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
}
