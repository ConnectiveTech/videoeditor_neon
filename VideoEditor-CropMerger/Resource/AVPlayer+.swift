//
//  AVPlayer+.swift
//  VideoEditor-CropMerger
//
//  Created by Bá Anh Nguyễn on 12/13/17.
//  Copyright © 2017 Bá Anh Nguyễn. All rights reserved.
//

import AVFoundation

extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}
